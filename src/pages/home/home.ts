import { PhotosProvider } from './../../providers/photos/photos';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ActionSheetController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';

import { PhotoModel } from '../../models/photoModel';

import 'rxjs/add/operator/map';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  photoListSvc : PhotoModel[];
  photoList : PhotoModel[];
  photoListBack : PhotoModel[];
  bCarregando : boolean = true;
  base64Image : string;

  searchTerm : string;
  shouldShowCancel : boolean = false;

  pgStart : number = 0;
  pgLimit : number = 5;
  pgTotal : number = 0;

  constructor(
    public navCtrl: NavController,
    public camera : Camera,
    public actionSheetCtrl: ActionSheetController,
    public photoProvider : PhotosProvider
  ) {

    photoProvider.getPhotos().subscribe((data)=>{
      this.pgTotal = data.length;
      this.photoListSvc = data;
      this.photoList = this.photoListSvc.slice(this.pgStart, this.pgLimit);
      this.photoListBack = this.photoList;
      this.bCarregando = false;
    });
  }

  listPicOptions() {
    this.actionSheetCtrl.create({
      title: 'Opções',
      buttons: [{
        text: 'Camera',
        handler: () => {
          this.takePicture();
        }
      },{
        text: 'Biblioteca',
        handler: () => {
          this.openLibrary();
        }
      },{
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    }).present();
  }

  takePicture() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      this.sendPicture();
    }, (err) => {
      console.log(err);
    });
  }

  openLibrary() {
    this.camera.getPicture({
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
      destinationType: this.camera.DestinationType.DATA_URL
     }).then((imageData) => {
       this.base64Image = 'data:image/jpeg;base64,' + imageData;
       this.sendPicture();
      }, (err) => {
       console.log(err);
     });
  }

  sendPicture() {
    console.log(this.base64Image);
  }

  onInput() {
    this.shouldShowCancel = true;
    this.photoList = this.photoList.filter((photo) => {
      return photo.title.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
    });
  }

  onCancel() {
    this.shouldShowCancel = false;
    this.photoList  = this.photoListBack;
    this.searchTerm = "";
  }

  doInfinite(infiniteScroll) {
    this.onCancel();

    this.pgStart++;
    this.photoList = this.photoListBack;

    return new Promise((resolve) => {
      setTimeout(() => {
        this.photoList = this.photoListSvc.slice(0, (this.pgStart + 1) * this.pgLimit);
        this.photoListBack = this.photoList;

        infiniteScroll.complete();
        resolve();
      }, 500);
    });

    
  }

}
